#ifndef GRID_OF_LIFE__POSITION
#define GRID_OF_LIFE__POSITION

#include <vector>

class Position {
public:

    short x;
    short y;

    Position() {}

    Position(short &x, short &y) {
        this->x = x;
        this->y = y;
    }

    Position(short x, short y) {
        this->x = x;
        this->y = y;
    }

    Position(const Position &p) {
        this->x = p.x;
        this->y = p.y;
    }

    Position operator+(const Position &p) {
        return Position(x + p.x, y + p.y);
    }

    bool operator==(const Position &other) {
        return ((this->x == other.x) && (this->y == other.y));
    }

    std::vector<Position> adjacent() {
        std::vector<Position> res = {Position(x + 1, y), Position(x - 1, y),
                                     Position(x, y + 1), Position(x, y - 1)};
        return res;
    }

};

#endif
