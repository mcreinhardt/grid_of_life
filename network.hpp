#ifndef GRID_OF_LIFE__NETWORK
#define GRID_OF_LIFE__NETWORK

#include <string>
#include <vector>
#include <list>
#include <unordered_map>

#include "neuron.hpp"
#include "laws.hpp"

class Network {

    std::vector<Neuron> net;

    unsigned short last_input_neuron;
    unsigned short last_output_neuron;

    unsigned short hidden_size;

    unsigned char current_step = 0;

    void forward_input(const std::vector<float> &inp, std::list<Neuron *> &have_fired);

    void fire(Neuron &n, float &energy, std::list<Neuron *> &fired_neurons_index);

    std::vector<float> collect_output();

    // mutational functions
    void delete_random_connections();

    void add_random_connections();

    void change_random_connections();

    void add_random_connected_neuron();

    void generate_connections();

    void add_connection(const unsigned short start_start, const unsigned short end_index, float weight);


public:

    Network() = default;

    Network(const unsigned short inputs, const unsigned short outputs);

    Network(const Network &parent);

    Network(const std::string);

    std::string to_string();

    void mutate();

    std::vector<float> think(const std::vector<float> &inp, float &energy);

    unsigned short get_hidden_size();
    unsigned short get_input_size();
    unsigned short get_output_size();
};

#endif
