/*
The Grid of Life

In the beginning there was nothing.
Then came God. And God was bored.
So he created the Grid. A large dark Plane full of nothing.
With his great almightyness he fills it with the Puckleporks, little
one-pixel-sized creatures, which are able to move in four directions.
After watching them randomly walking over his great Grid, he decides
that his tiny frinds are to carefree. He gave them an
insatiable hunger for small blobs he sometimes throw on the Grid.
But because they lack on vision, the most of the are starving.
So he gave them eyes, thrust and fairy-powder, and last but not least, a mind.
Now they are able to evolve to masters of the grid. But only the fittest can
survive in this rough space.
*/

#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <sstream>
#include <iomanip>

#include "disp_cimg.hpp"
#include "grid.hpp"
#include "laws.hpp"

using clock_ = std::chrono::steady_clock;
using duration_ = clock_::duration;

void redraw(Grid &world, CIMG_Display &screen) {
    unsigned char sd[] = {0, 0, 0};
    for (auto &inhabitant : world.inhabitants) {
        screen.draw_point(static_cast<unsigned short>(inhabitant.position.x),
                          static_cast<unsigned short>(inhabitant.position.y), sd);
    }
}

void draw(double biggest_puckle, double oldest_puckle, Grid &world, CIMG_Display &screen) {
    for (auto &inhabitant : world.inhabitants) {
        auto shade_of_gray = static_cast<unsigned char>(((inhabitant.blobs / biggest_puckle) * 200.0) + 50);
        auto age_shade = static_cast<unsigned char>(((inhabitant.get_age() / oldest_puckle) * 200.0) + 50);
        //unsigned char shade_of_gray = 255;
        unsigned char color[] = {shade_of_gray, age_shade, 0};
        screen.draw_point(static_cast<unsigned short>(inhabitant.position.x),
                          static_cast<unsigned short>(inhabitant.position.y), color);
    }
    screen.update_view();
}

duration_ sleep_till_next_frame(std::chrono::system_clock::time_point start) {
    using namespace std::chrono;
    auto duration = duration_cast<milliseconds>(system_clock::now() - start);
    if constexpr (laws::REGULATE_FRAMERATE) {
        auto to_sleep = (1000 / laws::MAX_FRAMERATE) - duration.count();
        if (to_sleep > 0)
            std::this_thread::sleep_for(milliseconds(to_sleep));
    }
    return duration;
}

auto average_move_duration(duration_ duration) {
    using namespace std::chrono;
    static int i = 0;
    static duration_ time_sum = 0ms;
    ++i;
    time_sum += duration;
    return time_sum / i;
}

void print_status(duration_ frame_time, unsigned int pucklepork_count, duration_ move_time) {
    using namespace std::chrono;
    if constexpr (laws::SHOW_ANYTHING) {
        static duration_ time_to_be_done = laws::FRAMERATE_DISPLAY_INTERVAL;
        time_to_be_done -= frame_time;
        if (time_to_be_done < 0ms) {

            time_to_be_done += laws::FRAMERATE_DISPLAY_INTERVAL;
            std::stringstream output;

            if constexpr (laws::SHOW_FRAMERATE)
                output << "FPS: "
                       << std::fixed
                       << std::setprecision(1)
                       << (1.0 / duration<double>(frame_time).count())
                       << ", ";

            if constexpr (laws::SHOW_PUCKLEPORKS_ALIVE_COUNT)
                output << "Puckleporks: " << pucklepork_count << ", ";
            if constexpr (laws::SHOW_CURRENT_SPACE_EFFICACY)
                output << "puckles/pixel: "
                       << static_cast<float>(laws::WORLD_HEIGHT * laws::WORLD_WIDTH) / pucklepork_count << ", ";
            if constexpr (laws::SHOW_CURRENT_CALCULATION_EFFICACY) {
                output << pucklepork_count
                       << " Puckleporks in "
                       << duration_cast<microseconds>(move_time).count()
                       << "µs, ";
            }

            auto str = output.str();

            std::cout << str.substr(0, str.size() - 2) << std::endl;
        }
    }
}

int main() {
    using namespace std::chrono;

    // make visible
    unsigned char background[] = {0, 0, 0};
    CIMG_Display screen =
            CIMG_Display(laws::WORLD_HEIGHT, laws::WORLD_WIDTH, background, laws::PUCKLEPORK_SCALE);

    // game loop
    Grid world = Grid();

    while (true) {
        // measure time for regulating frame rate
        auto start = system_clock::now();
        auto last_step_finished = start;
        duration_ average_step_time;
        // update board
        do {
            world.step();
            auto this_step_time = system_clock::now();
            average_step_time = average_move_duration(this_step_time - last_step_finished);

            last_step_finished = this_step_time;

        } while (duration_cast<milliseconds>(system_clock::now() - start).count() <
                 (1.0f / laws::MAX_FRAMERATE) * 1000);

        // display routine
        draw(world.find_biggest_puckle(), world.find_oldest_puckle(), world, screen);
        redraw(world, screen);

        auto time_for_frame = sleep_till_next_frame(start);
        // additional console output
        print_status(time_for_frame, world.get_inhabitants_size(), average_step_time);
        // quit if window is closed
        if (screen.window_active()) return 0;
    }
}

