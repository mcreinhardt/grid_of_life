#include <iostream>

#include "neuron.hpp"
#include "laws.hpp"
#include "helpers.hpp"

ActivationListElement::ActivationListElement(const unsigned char timestep, const float load) {
    this->timestep = timestep;
    this->load = load;
}

Neuron::Neuron() {
    activation_history = std::list<ActivationListElement>();
    connections = std::vector<int>();
    weights = std::vector<float>();
}

void Neuron::delete_timed_out_activations(const unsigned char current_step) {
    for (auto it = activation_history.begin(); it != activation_history.end(); ++it) {
        if (timestep_distance(current_step, it->timestep) < laws::ACTIVATION_LIFESPAN) return;
        it = activation_history.erase(it);
    }
}

float Neuron::activation(const unsigned char current_timestep) {
    float activation_sum = 0;
    for (auto &it : activation_history) {
        activation_sum += activation_over_time(timestep_distance(current_timestep, it.timestep)) * it.load;
    }
    return activation_sum;
}

void Neuron::clear_all_activations() {
    activation_history.clear();
}

void Neuron::add_new_activation(const unsigned char current_timestep, const float charge) {
    if (activation_history.size() > 0 && activation_history.back().timestep == current_timestep) {
        activation_history.back().load += charge;
    } else {
        activation_history.emplace_back(current_timestep, charge);
    }
}
