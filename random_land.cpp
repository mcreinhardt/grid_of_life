#include "random_land.hpp"

namespace random_land {

    std::random_device rd;
    std::mt19937 engine(rd());

    std::uniform_int_distribution<> random_mature_percentage(1, 99);
    std::uniform_int_distribution<> random_direction(0, 4);
    std::uniform_int_distribution<> random_bool(0, 1);

    std::uniform_int_distribution<> random_height(0, laws::WORLD_HEIGHT - 1);
    std::uniform_int_distribution<> random_width(0, laws::WORLD_WIDTH - 1);

    std::uniform_real_distribution<> random_connection(0, 1);

    std::uniform_real_distribution<> connection_deletion_probability(laws::MIN_DELETIONS_PER_NEURON,
                                                                 laws::MAX_DELETIONS_PER_NEURON);
    std::uniform_real_distribution<> new_connection_probability(laws::MIN_INSERTION_PER_NEURON,
                                                                  laws::MAX_INSERTION_PER_NEURON);
    std::uniform_real_distribution<> connection_change_probability(laws::MIN_CHANGES_PER_NEURON,
                                                               laws::MAX_CHANGES_PER_NEURON);

    std::uniform_real_distribution<> random_neuron_insertions(laws::MIN_DELETIONS_PER_NEURON,
                                                              laws::MAX_DELETIONS_PER_NEURON);
    std::uniform_real_distribution<> random_neuron_deletions(laws::MIN_DELETIONS_PER_NEURON,
                                                             laws::MAX_DELETIONS_PER_NEURON);

    float generat_random_float(float min, float max) {
        std::uniform_real_distribution<> random_delta(min, max);
        return random_delta(engine);
    }

    Position generate_random_position() {
        return Position(random_height(engine), random_width(engine));
    }

    int generate_random_int(int max) {
        std::uniform_int_distribution<> random_int(0, max - 1);
        return random_int(engine);
    }

    float random_connection_delta() {
        std::normal_distribution<> random_connection_changes(-laws::MAX_CONNECTION_CHANGE_DELTA / 2,
                                                             laws::MAX_CONNECTION_CHANGE_DELTA / 2);
        return random_connection_changes(engine);
    }

} // random_land
