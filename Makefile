.PHONY: clean

sources=*.cpp
exec=Executables/Game_of_Life
link=-lX11 -pthread

all: ${exec}

${exec}: ${sources}
	g++ ${sources} -o ${exec} -std=c++17 -O3  ${link}
	Executables/Game_of_Life

clean:
	rm *.gch *~
