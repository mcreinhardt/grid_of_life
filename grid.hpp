#ifndef GRID_OF_LIFE__GRID
#define GRID_OF_LIFE__GRID

#include <list>
#include <string>
#include <vector>
#include <iterator>
#include <random>

#include "position.hpp"
#include "pucklepork.hpp"

class Grid {
private:

    // if value reaches 1, a new pucklepork is spawned
    float spawn_next_pucklepork = 0;

    const std::list<Pucklepork>::iterator the_null;

    std::vector<std::vector<std::list<Pucklepork>::iterator>> pucklepork_index;
    std::vector<std::vector<unsigned char>> map;

    void time_to_kill_the_starving();

    void time_to_eat();

    void child_whished(Pucklepork &parent, Position &new_pos);

    bool other_smaller(Pucklepork &one, Pucklepork &other);

    Position find_position_for_child(Pucklepork &p);

    unsigned char get_maps_field(unsigned short x, unsigned short y);

    unsigned char get_maps_field(Position &p);

    bool flip_coin();

    std::list<Pucklepork>::iterator get_pucklepork(unsigned short x, unsigned short y);

    std::list<Pucklepork>::iterator get_pucklepork(Position &p);


public:

    Grid();

    Grid(std::string path);

    std::list<Pucklepork> inhabitants;

    void step();

    bool save(std::string path);
    bool load(std::string path);

    double find_biggest_puckle();

    double find_oldest_puckle();

    unsigned short get_inhabitants_size();

    unsigned short get_gridfield(Position p);

    std::vector<float> create_input(Pucklepork &p);

};

#endif
