#ifndef GRID_OF_LIFE__DISP_CIMG
#define GRID_OF_LIFE__DISP_CIMG

#include "CImg.h"

class CIMG_Display {
public:
    CIMG_Display(unsigned short width, unsigned short height,
                 unsigned char bg_color[3], unsigned char scale);

    void draw_point(unsigned short x, unsigned short y, unsigned char color[3]);

    void update_view();

    bool window_active();

private:
    cimg_library::CImgDisplay disp;
    cimg_library::CImg<unsigned char> img;

    unsigned char scale;
};

#endif
