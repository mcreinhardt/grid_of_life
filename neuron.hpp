#ifndef GRID_OF_LIFE__NEURON
#define GRID_OF_LIFE__NEURON

#include <vector>
#include <list>
#include "laws.hpp"

struct ActivationListElement {
    unsigned char timestep;
    float load;

    ActivationListElement(const unsigned char timestep, const float load);
};


class Neuron {
public:

    Neuron();

    void delete_timed_out_activations(const unsigned char current_timestep);

    float activation(const unsigned char current_timestep);

    void add_new_activation(const unsigned char current_timestep, const float charge);

    void clear_all_activations();

    std::list<ActivationListElement> activation_history;

    std::vector<int> connections;
    std::vector<float> weights;
    std::vector<int> neurons_backward;
};

#endif
