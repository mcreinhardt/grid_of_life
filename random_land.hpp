#ifndef GRID_OF_LIFE__RANDOM_LAND
#define GRID_OF_LIFE__RANDOM_LAND

#include <random>

#include "laws.hpp"
#include "position.hpp"

namespace random_land {
    extern std::random_device rd;
    extern std::mt19937 engine;

    extern std::uniform_int_distribution<> random_mature_percentage;
    extern std::uniform_int_distribution<> random_direction;
    extern std::uniform_int_distribution<> random_bool;


    extern std::uniform_int_distribution<> random_height;
    extern std::uniform_int_distribution<> random_width;

    extern std::uniform_real_distribution<> random_connection;

    extern std::uniform_real_distribution<> connection_deletion_probability;
    extern std::uniform_real_distribution<> new_connection_probability;
    extern std::uniform_real_distribution<> connection_change_probability;

    extern std::uniform_real_distribution<> random_neuron_insertions;
    extern std::uniform_real_distribution<> random_neuron_deletions;

    float generat_random_float(float min, float max);

    Position generate_random_position();

    int generate_random_int(int max);

    float random_connection_delta();

}
#endif
