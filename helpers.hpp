//
// Created by max on 19.11.18.
//

#ifndef GRID_OF_LIFE__HELPERS_HPP
#define GRID_OF_LIFE__HELPERS_HPP

#include <limits>
#include <cmath>

namespace detail {
    constexpr const auto min_char = std::numeric_limits<unsigned char>::min();
    constexpr const auto max_char = std::numeric_limits<unsigned char>::max();
}

inline unsigned char timestep_distance(const unsigned char earlier_timestep, const unsigned char later_timestep) noexcept {
    unsigned char distance_right;
    if(earlier_timestep < later_timestep){
        distance_right = later_timestep - earlier_timestep;
    } else {
        distance_right = earlier_timestep - later_timestep;
    }
    return (distance_right < detail::max_char/2) ? distance_right : detail::max_char-distance_right;
}

constexpr inline float activation_over_time(const unsigned char how_old) noexcept {
    return 1.0f / (1.0f + static_cast<float>(how_old));
}


#endif //GRID_OF_LIFE_HELPERS_HPP
