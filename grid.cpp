#include "grid.hpp"
#include "random_land.hpp"
#include "laws.hpp"

#include <iostream>
#include <thread>
#include <fstream>

Grid::Grid() {
    using namespace laws;
    pucklepork_index = std::vector<std::vector<std::list<Pucklepork>::iterator>>(
            WORLD_HEIGHT, std::vector<std::list<Pucklepork>::iterator>(
                    WORLD_WIDTH, the_null));
    // generation way is inefficient if ratio of puckleporks to all fields is nearly 1
    unsigned short puckleporks_generated = 0;
    while (puckleporks_generated < POPULATION_SIZE) {
        Position newbie_place = random_land::generate_random_position();
        if (get_pucklepork(newbie_place) == the_null) {
            // generate puckleporks with a neural net
            inhabitants.emplace_back(newbie_place);
            inhabitants.back().blobs += 1;
            ++puckleporks_generated;
        }
    }
    // currently only two options, but can be extendet...
    // 0 : no food
    // 1 : food
    map = std::vector<std::vector<unsigned char>>(WORLD_HEIGHT,
                                                  std::vector<unsigned char>(WORLD_WIDTH, 0));
    for (unsigned char j = 0; j < INITIAL_BLOB_CYCLES; ++j) {
        for (unsigned short i = 0;
             i < BLOB_SPAWN_RATE * WORLD_HEIGHT * WORLD_WIDTH;
             ++i) {
            Position new_blob = random_land::generate_random_position();
            map[new_blob.x][new_blob.y] = 1;
        }
    }

}

bool Grid::flip_coin() {
    return random_land::random_bool(random_land::engine);
}

bool on_board(Position &p) {
    return (p.x > -1 && p.x < laws::WORLD_HEIGHT && p.y > -1 &&
            p.y < laws::WORLD_WIDTH);
}

Position Grid::find_position_for_child(Pucklepork &p) {
    std::vector<Position> child_spawns = p.position.adjacent();
    static std::random_device rd;
    static std::mt19937 engine(rd());
    std::uniform_int_distribution<> random_int(0, 3);
    for (unsigned char i = 0; i < laws::RANDOMNESS; ++i) {
        for (unsigned char j = 0; j < 4; ++j) {
            unsigned short random = random_int(engine);
            Position buffer = child_spawns[random];
            child_spawns[random] = child_spawns[j];
            child_spawns[j] = buffer;
        }
    }
    for (unsigned short i = 0; i < 4; ++i) {
        if (on_board(child_spawns[i])) {
            if (get_pucklepork(child_spawns[i]) == the_null) {
                return child_spawns[i];
            }
        }
    }
    return Position(-1, -1);
}

bool Grid::other_smaller(Pucklepork &one, Pucklepork &other) {
    bool other_smaller = true;
    if (other.blobs > one.blobs) {
        other_smaller = false;
    } else if (other.blobs == one.blobs) {
        if (flip_coin())
            other_smaller = false;
    }
    return other_smaller;
}

void Grid::child_whished(Pucklepork &parent, Position &new_pos) {
    if (parent.blobs - laws::BIRTH_COST * 2 <= 0) return;
    Position birthplace = find_position_for_child(parent);
    if (birthplace.x != -1) {
        // create new puckle
        inhabitants.emplace_front(birthplace, parent);
        pucklepork_index[birthplace.x][birthplace.y] = inhabitants.begin();
        // give the new pucklepork food
        double blobs_for_child = parent.blobs * ((static_cast<double>(new_pos.y) / 100.0));
        inhabitants.front().blobs += blobs_for_child - laws::BIRTH_COST;
        parent.blobs -= blobs_for_child + laws::BIRTH_COST;
    }
}

void Grid::time_to_eat() {
    for (auto it = inhabitants.begin(); it != inhabitants.end(); ++it) {
        if (get_maps_field(it->position) == 1) {
            it->blobs += 1;
            map[it->position.x][it->position.y] = 0;
        }
    }
}

void Grid::time_to_kill_the_starving() {
    for (auto it = inhabitants.begin(); it != inhabitants.end(); ++it) {
        if (it->blobs < 0) {
            pucklepork_index[it->position.x][it->position.y] = the_null;
            it = --inhabitants.erase(it);
        }
    }
}

void thread_workload_capsule(const unsigned int start, const unsigned int count, std::vector<Position> *results,
                             std::list<Pucklepork> *inhabitants, Grid *g) {
    auto current_puckle = inhabitants->begin();
    for (int i = 0; i < start; ++i) {
        ++current_puckle;
    }

    for (int i = start; i < start + count; ++i) {
        (*results)[i] = current_puckle->move(g->create_input(*current_puckle));
        // movement costs
        ++current_puckle;
    }

}

/*
Checks at first if a pucklepork's move collides with to moved position of anoter
pucklepork. For that it creates an additional empty board on which only the new
move is written. When two collides, only the bigger survives and get % of the
saved blobs of the other.
*/

void Grid::step() {
    // generate random blobs

    for (int i = 0;
         i < laws::BLOB_SPAWN_RATE * laws::WORLD_HEIGHT * laws::WORLD_WIDTH;
         ++i) {
        Position new_blob = random_land::generate_random_position();
        map[new_blob.x][new_blob.y] = 1;
    }

    //check if its time to spawn new puckle
    spawn_next_pucklepork += laws::RANDOMS_RATE;
    while (spawn_next_pucklepork > 1) {
        spawn_next_pucklepork -= 1;
        inhabitants.emplace_back(random_land::generate_random_position());
        inhabitants.back().blobs = 1;
    }

    // clear all puckleporks from the board
    for (std::list<Pucklepork>::iterator it = inhabitants.begin();
         it != inhabitants.end(); ++it) {
        pucklepork_index[it->position.x][it->position.y] = the_null;
    }
    // divide inhabitants into groups for multithreading the neural nets
    std::vector<Position> new_positions = std::vector<Position>(inhabitants.size());
    std::vector<std::thread> threads;
    for (int i = 0; i < laws::THREAD_COUNT - 1; ++i) {
        threads.push_back(std::thread(thread_workload_capsule,
                                      (inhabitants.size() / laws::THREAD_COUNT) * i,
                                      inhabitants.size() / laws::THREAD_COUNT,
                                      &new_positions,
                                      &inhabitants,
                                      this));
    }
    // push the last part plus rest
    threads.push_back(std::thread(thread_workload_capsule,
                                  (inhabitants.size() / laws::THREAD_COUNT) * (laws::THREAD_COUNT - 1),
                                  inhabitants.size() / laws::THREAD_COUNT + inhabitants.size() % laws::THREAD_COUNT,
                                  &new_positions,
                                  &inhabitants,
                                  this));

    // wait for threads finish
    for (int i = 0; i < threads.size(); ++i) {
        threads[i].join();
    }

    // try to set all puckleporks to their next desired place
    auto new_pos = new_positions.begin();
    for (auto it = inhabitants.begin(); it != inhabitants.end(); ++it) {

        if (new_pos->x == -42) {
            child_whished(*it, *new_pos);
        }
        if (!on_board(*new_pos)) {
            *new_pos = it->position;
        }
        // no other pucklepork has moved there?
        if (get_pucklepork(*new_pos) == the_null) {
            it->position = Position(*new_pos);
            pucklepork_index[new_pos->x][new_pos->y] = it;
        } else {
            // the smaller will die
            if (other_smaller(*it, *get_pucklepork(*new_pos))) {
                it->blobs += get_pucklepork(*new_pos)->blobs * laws::BLOB_RECYCLE_RATE;
                it->position = Position(*new_pos);
                inhabitants.erase(get_pucklepork(*new_pos));
                pucklepork_index[new_pos->x][new_pos->y] = it;
            } else {
                get_pucklepork(*new_pos)->blobs += it->blobs * laws::BLOB_RECYCLE_RATE;
                it = --inhabitants.erase(it);
            }
        }
        ++new_pos;
    }
    // eating must be done separately when fights are done, otherwise food can be
    // lost due recycling food of looser instead of giving it directly the winner
    time_to_eat();

    time_to_kill_the_starving();
}

unsigned char Grid::get_maps_field(unsigned short x, unsigned short y) { return map[x][y]; }

unsigned char Grid::get_maps_field(Position &p) { return map[p.x][p.y]; }

std::list<Pucklepork>::iterator Grid::get_pucklepork(unsigned short x, unsigned short y) {
    return pucklepork_index[x][y];
}

std::list<Pucklepork>::iterator Grid::get_pucklepork(Position &p) {
    return pucklepork_index[p.x][p.y];
}

bool Grid::save(std::string path) {
    std::ofstream file;
    file.open(path, std::ios::out | std::ios::trunc);

    if(!file){
        std::cout << "Something gone wrong, cannot write file!" << std::endl;
        return false;
    }

    file << spawn_next_pucklepork << ' ';

    file << map.size() << ' ' << map[0].size() << '\n';

    for(auto& row : map){
        for(auto& field : row){
            file << static_cast<int>(field);
        }
    }
    file << '\n';

    for(auto& puckle : inhabitants){
        file << puckle.position.x << ' ' << puckle.position.y << ' ';
        file << puckle.blobs << ' ';
        file << puckle.get_age() << '\n';
        file << puckle.get_brain_as_text() << '\n';
    }

    return true;
}

double Grid::find_biggest_puckle() {
    double biggest_blob = 0;
    for (auto &inhabitant : inhabitants) {
        if (inhabitant.blobs > biggest_blob)
            biggest_blob = inhabitant.blobs;
    }
    return biggest_blob;
}

double Grid::find_oldest_puckle() {
    double oldest = 0;
    for (auto &inhabitant : inhabitants) {
        if (inhabitant.get_age() > oldest)
            oldest = inhabitant.get_age();
    }
    return oldest;
}

/*
 * The number of the biggest option must be always equal to laws::INPUT_DIVISOR
 */

unsigned short Grid::get_gridfield(Position p) {
    if (!on_board(p))
        return 1;
    else if (get_pucklepork(p) != the_null)
        return 2;
    else if (get_maps_field(p) == 1)
        return 3;
    else
        return 0;
}

unsigned short Grid::get_inhabitants_size() {return static_cast<unsigned short>(inhabitants.size());}

std::vector<float> Grid::create_input(Pucklepork &p) {
    std::vector<float> input = std::vector<float>(p.get_input_size());
    int c = -1;
    for (int width = laws::MAX_EYE_SCATTERING; width > 0; --width) {
        for (int i = 1; i <= laws::MAX_EYE_SCATTERING - abs(width); ++i) {
            input[++c] = static_cast<float>(get_gridfield(p.position + Position(width, i))) / laws::INPUT_DIVISOR;
            input[++c] = static_cast<float>(get_gridfield(p.position + Position(width, -i))) / laws::INPUT_DIVISOR;
            input[++c] = static_cast<float>(get_gridfield(p.position + Position(-width, i))) / laws::INPUT_DIVISOR;
            input[++c] = static_cast<float>(get_gridfield(p.position + Position(-width, -i))) / laws::INPUT_DIVISOR;
        }
    }
    for (int i = 1; i <= laws::MAX_EYE_SCATTERING; ++i) {
        input[++c] = static_cast<float>(get_gridfield(p.position + Position(i, 0))) / laws::INPUT_DIVISOR;
        input[++c] = static_cast<float>(get_gridfield(p.position + Position(-i, 0))) / laws::INPUT_DIVISOR;
        input[++c] = static_cast<float>(get_gridfield(p.position + Position(0, i))) / laws::INPUT_DIVISOR;
        input[++c] = static_cast<float>(get_gridfield(p.position + Position(0, -i))) / laws::INPUT_DIVISOR;
    }

    input[c] = p.blobs;

    return input;
}
