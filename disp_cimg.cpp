#include "disp_cimg.hpp"

CIMG_Display::CIMG_Display(unsigned short width, unsigned short height,
                           unsigned char bg_color[3], unsigned char scale) {
  this->scale = scale;
  img.assign(1, 1, 1, 3, 0).resize(width * scale, height * scale);
  disp = cimg_library::CImgDisplay(img, "Grid Of Life");
};

void CIMG_Display::draw_point(unsigned short x, unsigned short y,
                              unsigned char color[3]) {
  for (unsigned short i = 0; i < scale; ++i) {
    if(x * scale + i < img.width()){
      for (unsigned short j = 0; j < scale; ++j) {
        if(y * scale + j < img.height()){
          img.draw_point(x * scale + i, y * scale + j, color);
        }
      }
    }
  }
}

void CIMG_Display::update_view() { disp.display(img); }

bool CIMG_Display::window_active() {
  return disp.is_closed();
}
