#include <algorithm>
#include <iostream>

#include "pucklepork.hpp"
#include "random_land.hpp"
#include "laws.hpp"

void Pucklepork::initialize_brain() {
    brain = Network(2 * laws::MAX_EYE_SCATTERING * (laws::MAX_EYE_SCATTERING + 1) + 1, 3);
}

Pucklepork::Pucklepork(const Position &pos) {
    position = Position(pos);
    initialize_brain();
}

Pucklepork::Pucklepork(const Position &pos, const Pucklepork &parent) {
    position.x = pos.x;
    position.y = pos.y;

    brain = Network(parent.brain);

    brain.mutate();
}


Pucklepork::Pucklepork(const Pucklepork &a) {
    blobs = a.blobs;
    brain = Network(a.brain);
    position = a.position;
}

std::string Pucklepork::get_brain_as_text() {
    return brain.to_string();
}

/*
    Outputs:
    0: directions
    1: child
    2: spent energy for child
    Inputs:
    1: energy
    2-x eyes
*/
Position Pucklepork::move(const std::vector<float> &vision) {
    std::vector<float> brains_output = brain.think(vision, blobs);

    ++age;

    blobs -= laws::MIN_STEP_COST;
    blobs -= brain.get_hidden_size() * laws::STEP_COST_PER_NEURON;

    // say you want a child
    if (brains_output[1] > 0.5 && brains_output[2] < 1) {
        return Position(-42, brains_output[2] * 100.0);
    }

    // movement
    if (brains_output[0] > 0.8) {
        return Position(position.x + 1, position.y);
    } else if (brains_output[0] > 0.6) {
        return Position(position.x - 1, position.y);
    } else if (brains_output[0] > 0.4) {
        return Position(position.x, position.y + 1);
    } else if (brains_output[0] > 0.2) {
        return Position(position.x, position.y - 1);
    } else {
        return position;
    }
}

Position Pucklepork::move_random() {
    Position res = Position(position);
    switch (random_land::random_direction(random_land::engine)) {
        case 0:
            res.x += 1;
            return res;
        case 1:
            res.x -= 1;
            return res;
        case 2:
            res.y -= 1;
            return res;
        case 3:
            res.y += 1;
            return res;
    }
    res = Position(-42, random_land::random_mature_percentage(random_land::engine));
    return res;
}

unsigned short Pucklepork::get_age() {
    return age;
}

unsigned short Pucklepork::get_hidden_size() {
    return brain.get_hidden_size();
}

unsigned short Pucklepork::get_input_size() {
    return brain.get_input_size();
}

unsigned short Pucklepork::get_output_size() {
    return brain.get_output_size();
}
