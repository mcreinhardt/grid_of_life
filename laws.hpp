#ifndef GRID_OF_LIFE__LAWS
#define GRID_OF_LIFE__LAWS

#include <list>
#include <chrono>

namespace laws {
    // GENERAL:
    // how many puckleporks start
    const float PUCKLEPORK_DENSITY = 0.05;
    // how much costs life
    const float MIN_STEP_COST = 0.1;
    const float STEP_COST_PER_NEURON = 0.001;
    const float BIRTH_COST = 0.125;
    const float ACTIVATION_COST = 0.001;
    // how many percent of the eaten blob are added to the blob of the other
    const float BLOB_RECYCLE_RATE = 0.1;
    // spawn rate per pixel of grid per cycle
    const float BLOB_SPAWN_RATE = 0.05;
    // to feed the first generation, extra blos are generated: BLOB_SPAWN_RATE * INITIAL_BLOB_CYCLES
    const unsigned char INITIAL_BLOB_CYCLES = 5;


    // WINDOW:
    const bool REGULATE_FRAMERATE = false;

    const unsigned int MAX_FRAMERATE = 50;

    const unsigned short WORLD_HEIGHT = 50;
    const unsigned short WORLD_WIDTH = 50;

    const unsigned short PUCKLEPORK_SCALE = 5;

    // Multitheading
    const unsigned int THREAD_COUNT = 8;

    // CONSOLE INFORMATIONS
    const bool SHOW_ANYTHING = true;
    const bool SHOW_FRAMERATE = true;
    const bool SHOW_CURRENT_SPACE_EFFICACY = false;
    const bool SHOW_CURRENT_CALCULATION_EFFICACY = true;
    const auto FRAMERATE_DISPLAY_INTERVAL = std::chrono::milliseconds(4000);
    const bool SHOW_PUCKLEPORKS_ALIVE_COUNT = false;


    // NEURAL NET
    // Activation Lifespan in Cycle
    const unsigned char ACTIVATION_LIFESPAN = 5;

    // must be smaller than sizeof(unsigned char)/2 !!! see @distance()
    const unsigned char STEPS_PER_CYCLE = 10;

    // these are the default values which are used for creating new lifeforms
    const float MAX_CONNECTION_CHANGE_DELTA = 0.5;

    const float MIN_INSERTION_PER_NEURON = 1.0f / 200;
    const float MAX_INSERTION_PER_NEURON = 1.0f / 10;

    const float MIN_DELETIONS_PER_NEURON = 1.0f / 100;
    const float MAX_DELETIONS_PER_NEURON = 1.0f / 10;

    const float MIN_CHANGES_PER_NEURON = 1.0f / 50;
    const float MAX_CHANGES_PER_NEURON = 1.0f / 20;

    const char MAX_EYE_SCATTERING = 2;

    // REPRODUCTION:
    // the percentage of completely new variants after no one is alive anymore
    const float RANDOMS_RATE = 0.1;
    // the initial hidden layer size is calculated: output_size + (1/RHO)*input_size
    const float RHO = 1;

    // SOME OTHER STUFF
    // how many swaps are made
    const unsigned char RANDOMNESS = 3;

    const float INPUT_DIVISOR = 3;

    // DO NOT MODIFY!
    const unsigned int POPULATION_SIZE =
            static_cast<unsigned int>(static_cast<double>(WORLD_WIDTH) * static_cast<double>(WORLD_HEIGHT) *
                                      PUCKLEPORK_DENSITY);
    const float THRESHOLD = 1;
}

#endif
