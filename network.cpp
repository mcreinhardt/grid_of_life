#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>

#include "laws.hpp"
#include "network.hpp"
#include "random_land.hpp"
#include "helpers.hpp"

void Network::generate_connections() {
    // connect hidden layer to input
    auto upperbound_input_weight = 9.0f / get_input_size();
    for (unsigned short i = 0; i <= last_input_neuron; ++i) {
        for (unsigned short j = last_output_neuron + 1; j < net.size(); ++j) {
            add_connection(i, j, random_land::generat_random_float(0, upperbound_input_weight));
        }
    }
    // connect to output
    for (unsigned short i = last_output_neuron + 1; i < net.size(); ++i) {
        for (unsigned short j = last_input_neuron + 1; j <= last_output_neuron; ++j) {
            add_connection(i, j, random_land::generat_random_float(0, 1));
        }
    }
}

Network::Network(unsigned short inputs, unsigned short outputs) {
    // generate output and inputs
    last_input_neuron = inputs - 1;
    last_output_neuron = last_input_neuron + outputs;
    hidden_size = (1 / laws::RHO) * inputs;
    net = std::vector<Neuron>(last_output_neuron + hidden_size + 1, Neuron());
    generate_connections();
}

Network::Network(const Network &parent){
    current_step = parent.current_step;
    last_input_neuron = parent.last_input_neuron;
    last_output_neuron = parent.last_output_neuron;
    hidden_size = parent.hidden_size;
    net.reserve(parent.net.size());
    for (unsigned short i = 0; i < parent.net.size(); ++i) {
        net.emplace_back(parent.net[i]);
    }
}

Network::Network(const std::string s){
    std::ifstream file;
    file.open(s);

    if(!file){
        std::cout << "Cannot open file!"<< std::endl;
        Network();
        return;
    }

    int x,y, neuron_count;

    file >> neuron_count >> x >> y;

    net.reserve(neuron_count);

}

std::string Network::to_string() {
    std::stringstream res;
    res << net.size() << ' ' << last_input_neuron << ' ' << last_output_neuron << ' ' << static_cast<int>(current_step) << '\n';
    for(auto& neuron : net){
        for(int i = 0; i < neuron.connections.size(); ++i){
            res << neuron.connections[i] << ' ' << neuron.weights[i] << ' ';
        }
        for(int i = 0; i < neuron.neurons_backward.size(); ++i){
            res << neuron.neurons_backward[i] << ' ';
        }
        for(auto& i : neuron.activation_history){
            res << i.load << ' ' << static_cast<int>(i.timestep) << ' ';
        }
    }
    return res.str();
}

void Network::mutate() {
    change_random_connections();
    delete_random_connections();
    add_random_connections();
}

void Network::add_connection(unsigned short start_index, unsigned short end_index, float weight) {
    net[end_index].neurons_backward.emplace_back(start_index);
    net[start_index].connections.emplace_back(end_index);
    net[start_index].weights.emplace_back(weight);
}

void Network::forward_input(const std::vector<float> &inp, std::list<Neuron *> &have_fired) {
    for (unsigned short i = 0; i < inp.size(); ++i) {
        for (unsigned short j = 0; j < net[i].connections.size(); ++j) {
            const int target = net[i].connections[j];
            net[target].delete_timed_out_activations(current_step);
            if(inp[i] == 0){
                continue;
            }
            net[target].add_new_activation(current_step, inp[i]);

            if (auto strength = net[target].activation(current_step);
                    strength > laws::THRESHOLD && strength - net[i].weights[j] <= laws::THRESHOLD) {
                have_fired.emplace_back(&net[net[i].connections[j]]);
            }
        }
    }
}

std::vector<float> Network::collect_output() {
    std::vector<float> result = std::vector<float>(last_output_neuron - last_input_neuron);
    for (unsigned char i = last_input_neuron + 1; i <= last_output_neuron; ++i) {
        net[i].delete_timed_out_activations(current_step);
        result[i - last_input_neuron - 1] = net[i].activation(current_step);
    }
    return result;
}

void Network::fire(Neuron &n, float &energy, std::list<Neuron *> &fired_neurons) {
    energy -= laws::ACTIVATION_COST;
    n.clear_all_activations();
    int connection = 0;
    for (auto i : n.connections) {
        net[i].delete_timed_out_activations(current_step);
        net[i].add_new_activation(current_step, n.weights[connection]);
        if (auto activation = net[i].activation(current_step); activation > laws::THRESHOLD &&
                                                               activation - n.weights[connection] < laws::THRESHOLD) {
            fired_neurons.emplace_back(&net[i]);
        }
        ++connection;
    }
}

std::vector<float> Network::think(const std::vector<float> &inp, float &energy) {
    std::list<Neuron *> have_fired = std::list<Neuron *>();
    forward_input(inp, have_fired);
    Neuron *end_of_step_marker = have_fired.back();
    unsigned char initial_step = current_step;
    auto it = have_fired.begin();
    while (energy > 0 && timestep_distance(current_step, initial_step) < laws::STEPS_PER_CYCLE &&
           have_fired.size() > 0) {

        fire(**it, energy, have_fired);

        if (end_of_step_marker == *it) {
            it = have_fired.erase(have_fired.begin(), ++it);
            ++current_step;
            end_of_step_marker = have_fired.back();
        } else {
            ++it;
        }
    }

    return collect_output();
}

/*
chooses at first randomly a hidden neuron. THen it randomly choosen one of its connections and removes this
*/
void Network::delete_random_connections() {
    using namespace random_land;
    auto neuron_deletions = static_cast<unsigned short>(static_cast<float>(hidden_size) *
                                                        connection_deletion_probability(engine));
    for (unsigned short i = 0; i < neuron_deletions; ++i) {
        auto selected = static_cast<unsigned short>(last_output_neuron + generate_random_int(hidden_size));

        if (net[selected].connections.size() <= 0)
            break;

        auto removed_connection = static_cast<unsigned short>(random_land::generate_random_int(
                net[selected].connections.size()));

        // delete backwards link
        for (auto it = net[selected].neurons_backward.begin(); it != net[selected].neurons_backward.end(); ++it) {
            if (*it == selected) {
                net[selected].neurons_backward.erase(it);
                break;
            }
        }
        // delete forward link
        auto address = net[selected].connections.begin();
        auto strength = net[selected].weights.begin();
        while (removed_connection > 0) {
            --removed_connection;
            ++address;
            ++strength;
        }
        net[selected].connections.erase(address);
        net[selected].weights.erase(strength);
    }
}

void Network::add_random_connections() {
    using namespace random_land;
    unsigned short neuron_insertions =
            hidden_size * new_connection_probability(engine);
    for (int i = 0; i < neuron_insertions; ++i) {
        add_connection(last_output_neuron + 1 + generate_random_int(hidden_size),
                       last_output_neuron + 1 + generate_random_int(hidden_size), random_connection(engine));
    }
}

/*
For performance reasons this method changes not completely random connections.
Connections from Neurons with many connections are less likely chosen for
mutation.
*/
void Network::change_random_connections() {
    unsigned short connection_changes_count = hidden_size * random_land::connection_change_probability(random_land::engine);
    for (unsigned short i = 0; i < connection_changes_count; ++i) {

        unsigned short hidden_indice = last_output_neuron + 1 + random_land::generate_random_int(hidden_size);

        if (net[hidden_indice].connections.size() == 0)
            continue;

        net[hidden_indice].weights[random_land::generate_random_int(
                net[hidden_indice].connections.size())] +=
                random_land::random_connection_delta();
    }
}

unsigned short Network::get_hidden_size() {
    return hidden_size;
}

unsigned short Network::get_input_size() {
    return last_input_neuron + 1;
}

unsigned short Network::get_output_size() {
    return last_output_neuron - last_input_neuron;
}
