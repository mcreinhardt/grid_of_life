#ifndef GRID_OF_LIFE__PUCKLEPORK
#define GRID_OF_LIFE__PUCKLEPORK

#include <random>
#include <string>

#include "position.hpp"
#include "network.hpp"

class Pucklepork {
private:

    Network brain;

    unsigned short age = 0;

    void initialize_brain();

    Pucklepork(Pucklepork &&) = delete;

public:

    float blobs = 0;
    Position position;

    Pucklepork(const Pucklepork &);

    Pucklepork(const Position &pos);

    Pucklepork(const Position &pos, const Pucklepork &parent);

    unsigned short get_age();

    unsigned short get_input_size();
    unsigned short get_output_size();
    unsigned short get_hidden_size();

    std::string get_brain_as_text();

    Position move(const std::vector<float> &vision);

    Position move_random();
};

#endif
